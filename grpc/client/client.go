package client

import (
	"catalog_api_gateway/config"
	pbc "catalog_api_gateway/genproto/catalog_service"
	"google.golang.org/grpc"
)

type IServiceManger interface {
	CategoryService() pbc.CategoryServiceClient
	ProductService() pbc.ProductServiceClient
}

type grpcClients struct {
	categoryService pbc.CategoryServiceClient
	productService  pbc.ProductServiceClient
}

func NewGrpcClients(cfg config.Config) (IServiceManger, error) {
	connCategoryService, err := grpc.Dial(
		cfg.AdminGRPCServiceHost+cfg.AdminGRPCServicePort,
		grpc.WithInsecure(),
	)
	if err != nil {
		return nil, err
	}

	return &grpcClients{
		categoryService: pbc.NewCategoryServiceClient(connCategoryService),
		productService:  pbc.NewProductServiceClient(connCategoryService),
	}, nil
}

func (g *grpcClients) CategoryService() pbc.CategoryServiceClient {
	return g.categoryService
}

func (g *grpcClients) ProductService() pbc.ProductServiceClient {
	return g.productService
}
