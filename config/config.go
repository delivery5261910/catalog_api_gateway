package config

import (
	"fmt"
	"github.com/joho/godotenv"
	"github.com/spf13/cast"
	"os"
)

type Config struct {
	ServiceName string
	Environment string

	HTTPPort string

	AdminGRPCServiceHost string
	AdminGRPCServicePort string
}

func Load() Config {
	if err := godotenv.Load(); err != nil {
		fmt.Println("error!!!", err)
	}

	cfg := Config{}

	cfg.ServiceName = cast.ToString(getOrReturnDefault("SERVICE_NAME", "store"))
	cfg.Environment = cast.ToString(getOrReturnDefault("ENVIRONMENT", "dev"))

	cfg.HTTPPort = cast.ToString(getOrReturnDefault("HTTP_PORT", ":8080"))

	cfg.AdminGRPCServiceHost = cast.ToString(getOrReturnDefault("TODO_GRPC_SERVICE_HOST", "localhost"))
	cfg.AdminGRPCServicePort = cast.ToString(getOrReturnDefault("TODO_GRPC_SERVICE_PORT", ":8001"))

	return cfg
}

func getOrReturnDefault(key string, defaultValue interface{}) interface{} {
	value := os.Getenv(key)
	if value != "" {
		return value
	}

	return defaultValue
}
