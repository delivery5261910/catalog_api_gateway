package models

type CreateCategory struct {
	Title    string `json:"title"`
	Status   bool   `json:"status"`
	ParentID string `json:"parent_id"`
}

type Category struct {
	ID       string `json:"id"`
	Title    string `json:"title"`
	Status   bool   `json:"status"`
	ParentID string `json:"parent_id"`
}

type CategoryResponse struct {
	Categories []Category `json:"categories"`
	Count      int        `json:"count"`
}
