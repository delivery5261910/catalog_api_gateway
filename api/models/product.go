package models

type CreateProduct struct {
	Description string  `json:"description"`
	Status      bool    `json:"status"`
	Type        string  `json:"type"`
	Price       float64 `json:"price"`
	CategoryID  string  `json:"category_id"`
}

type Product struct {
	ID          string  `json:"id"`
	Description string  `json:"description"`
	OrderNumber string  `json:"order_number"`
	Status      bool    `json:"status"`
	Type        string  `json:"type"`
	Price       float64 `json:"price"`
	CategoryID  string  `json:"category_id"`
}

type ProductResponse struct {
	Products []Product `json:"products"`
	Count    int       `json:"count"`
}
